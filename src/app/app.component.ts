import { Component } from '@angular/core';
import { Device } from '@ionic-native/device/ngx';
import { Platform } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wc-ionic';

  constructor(
    public platform: Platform,
    public device: Device,
    private diagnostic: Diagnostic
  ) {
    console.log('Device platform is: ' + this.device.platform);
  }

  clickMe() {
    console.log("this.platform.is('hybrid')", this.platform.is('hybrid'));
    console.log('this.platform.is', this.platform.is);
    console.log('this.platform.height()', this.platform.height());

  }

  diagnostico() {
    let successCallback = (isAvailable) => { console.log('Is available? ' + isAvailable); }
    let errorCallback = (e) => console.error(e);

    this.diagnostic.isCameraAvailable().then(successCallback).catch(errorCallback);

    this.diagnostic.isBluetoothAvailable().then(successCallback, errorCallback);


    this.diagnostic.getBluetoothState()
      .then((state) => {
        if (state == this.diagnostic.bluetoothState.POWERED_ON) {
          // do something
        } else {
          // do something else
        }
      }).catch(e => console.error(e));
  }
}
