const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
    // IE
    const filesEs5 = [
        './dist/wc-ionic/polyfills-es5.js',
        './dist/wc-ionic/main-es5.js'
    ];

    const filesEs2015 = [
        './dist/wc-ionic/polyfills-es2015.js',
        './dist/wc-ionic/polyfill-webcomp.js',
        './dist/wc-ionic/main-es2015.js'
    ];


    await fs.ensureDir('angular-elements');

    await concat(filesEs5, 'angular-elements/wc-elements-es5.js');
    await concat(filesEs2015, 'angular-elements/wc-elements-es2015.js');

    await fs.copyFile(
        './dist/argo-webcomponent/favicon.ico',
        'angular-elements/favicon.ico'
    );
})();